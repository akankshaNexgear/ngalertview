//
//  ViewController.m
//  NGAlertView
//
//  Created by Akanksha Sharma on 15/10/15.
//  Copyright © 2015 Akanksha Sharma. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showOneBtnAlertView:(id)sender {
    NSMutableArray *btnArr = [[NSMutableArray alloc] init];
    NGAlertButton *btn1 = [[NGAlertButton alloc] init];
    //btn1.showBtnText = [NSNumber numberWithBool:1];
    [btn1 setImage:[UIImage imageNamed:@"frodo_x.png"] forState:UIControlStateNormal];
    [btnArr addObject:btn1];
    NGAlertView *alertView = [[[NSBundle mainBundle] loadNibNamed:@"NGAlertView" owner:self options:nil] objectAtIndex:0];
    [alertView showAlertViewWithAlertMessage:@"This is a TEST MESSAGE" WithButtons:btnArr withButtonClickedBlock:^(int tag) {
        NSLog(@"%i",tag);
    }];
    
}

- (IBAction)showTwoBtnAlertView:(id)sender {
    NSMutableArray *btnArr = [[NSMutableArray alloc] init];
    NGAlertButton *btn1 = [[NGAlertButton alloc] init];
    [btn1 setImage:[UIImage imageNamed:@"frodo_x.png"] forState:UIControlStateNormal];
    [btnArr addObject:btn1];
    NGAlertButton *btn2 = [[NGAlertButton alloc] init];
    [btn2 setImage:[UIImage imageNamed:@"frodo_x.png"] forState:UIControlStateNormal];
    [btnArr addObject:btn2];
    NGAlertView *alertView = [[[NSBundle mainBundle] loadNibNamed:@"NGAlertView" owner:self options:nil] objectAtIndex:0];
    alertView.showAlertCloseButton = [NSNumber numberWithBool:true];
    [alertView showAlertViewWithAlertMessage:nil WithButtons:btnArr withButtonClickedBlock:^(int tag) {
        NSLog(@"%i",tag);
    }];
    
}

- (IBAction)showThreeBtnAlertView:(id)sender {
    NSMutableArray *btnArr = [[NSMutableArray alloc] init];
    NGAlertButton *btn1 = [[NGAlertButton alloc] initButtonWithBackgroundColor:[UIColor redColor] withBtnText:@"One" withImageName:@"frodo_x.png"];
    [btnArr addObject:btn1];
    
    NGAlertButton *btn2 = [[NGAlertButton alloc] initButtonWithBackgroundColor:[UIColor blueColor] withBtnText:@"DISCARD & START OVER" withImageName:@"frodo_x.png"];
    [btnArr addObject:btn2];
    
    NGAlertButton *btn3 = [[NGAlertButton alloc] initButtonWithBackgroundColor:[UIColor grayColor] withBtnText:@"three" withImageName:@"frodo_x.png"];
    [btnArr addObject:btn3];
    
    NGAlertView *alertView = [[[NSBundle mainBundle] loadNibNamed:@"NGAlertView" owner:self options:nil] objectAtIndex:0];
    alertView.showAlertCloseButton = [NSNumber numberWithBool:true];
    [alertView showAlertViewWithAlertMessage:@"Are you sure you want to delete this media?"  WithButtons:btnArr withButtonClickedBlock:^(int tag) {
        NSLog(@"%i",tag);
        if(tag == 0){
            [alertView closeButton:sender];
        }
    }];
}

- (IBAction)showNotifications:(id)sender {
    
    NSMutableArray *btnArr = [[NSMutableArray alloc] init];
    NGAlertView *alertView = [[[NSBundle mainBundle] loadNibNamed:@"NGAlertView" owner:self options:nil] objectAtIndex:0];
    alertView.showAlertCloseButton = [NSNumber numberWithBool:true];
    [alertView showAlertViewWithAlertMessage:@"This is a test notification"WithButtons:btnArr withButtonClickedBlock:^(int tag) {
        [alertView removeFromSuperview];
        if(tag == 1){
        }
        NSLog(@"%i",tag);
    }];
}
@end
