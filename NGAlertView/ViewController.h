//
//  ViewController.h
//  NGAlertView
//
//  Created by Akanksha Sharma on 15/10/15.
//  Copyright © 2015 Akanksha Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGAlertButton.h"
#import "NGAlertView.h"

@interface ViewController : UIViewController

- (IBAction)showOneBtnAlertView:(id)sender;
- (IBAction)showTwoBtnAlertView:(id)sender;
- (IBAction)showThreeBtnAlertView:(id)sender;
- (IBAction)showNotifications:(id)sender;

@end

