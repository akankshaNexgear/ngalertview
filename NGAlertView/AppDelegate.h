//
//  AppDelegate.h
//  NGAlertView
//
//  Created by Akanksha Sharma on 15/10/15.
//  Copyright © 2015 Akanksha Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

