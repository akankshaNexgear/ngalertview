//
//  NGAlertButton.h
//  NGAlertView
//
//  Created by Akanksha Sharma on 15/10/15.
//  Copyright © 2015 Akanksha Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGAlertButton : UIButton{
}

- (id) initButtonWithBackgroundColor : (UIColor *) bgColor withBtnText : (NSString *) btnText withImageName : (NSString *) btnImageName;
@property (nonatomic,copy) NSString *btnText;


@end
