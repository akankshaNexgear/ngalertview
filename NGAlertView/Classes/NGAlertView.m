//
//  FrodoAlertView.m
//  Frodo
//
//  Created by Akanksha Sharma on 24/09/15.
//  Copyright (c) 2015 Nexgear Technology Pvt. Ltd. All rights reserved.
//

#import "NGAlertView.h"

@implementation NGAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) showAlertViewWithAlertMessage : (NSString *) alertMessage WithButtons : (NSMutableArray *) alertButtons withButtonClickedBlock : (BtnClickedWithTag) btnClickWithTag {
    _btnClickedWithTag = btnClickWithTag;
    [self createButtonsViewWithButton:alertButtons];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    self.frame = [UIApplication sharedApplication].keyWindow.frame;
    self.alpha = 0;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1;
    }];
    self.center = [UIApplication sharedApplication].keyWindow.center;
}


- (void) createButtonsViewWithButton : (NSMutableArray *) alertButtons{
    self.buttonViewHeight.constant = 30 + (40 * [alertButtons count]) + (15 * ([alertButtons count] - 1));
    _buttonsViewWidth.constant = [[UIApplication sharedApplication] keyWindow].frame.size.width;
    CGFloat originY = 15;
    for (int i = 0; i < [alertButtons count]; i++) {
        NGAlertButton *btn = (NGAlertButton *)[alertButtons objectAtIndex:i];
        btn.frame = CGRectMake((_buttonsViewWidth.constant - 250)/2, originY, 250 , 40);
        if(i == ([alertButtons count] - 1)){
            btn.tag = 0;
            btn.layer.borderWidth = 2.0;
            btn.layer.borderColor = [UIColor whiteColor].CGColor;
            [btn setBackgroundColor:[UIColor clearColor]];
            
        } else {
            btn.tag = i + 1;
        }
        btn.layer.cornerRadius = 5;
        [btn addTarget:self action:@selector(alertButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonsView addSubview:btn];
        originY = originY + 65;
    }
}

- (IBAction)closeButton:(id)sender {
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (IBAction)alertButtonClicked:(id)sender {
    NGAlertButton *btn = (NGAlertButton *)sender;
    _btnClickedWithTag((int)btn.tag);
}

@end
