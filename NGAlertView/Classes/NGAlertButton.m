//
//  NGAlertButton.m
//  NGAlertView
//
//  Created by Akanksha Sharma on 15/10/15.
//  Copyright © 2015 Akanksha Sharma. All rights reserved.
//

#import "NGAlertButton.h"

@implementation NGAlertButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id) initButtonWithBackgroundColor : (UIColor *) bgColor withBtnText : (NSString *) btnText withImageName : (NSString *) btnImageName{
    
    self = [super init];
    if(self){
        self.backgroundColor = bgColor;
        //[self.imageView setContentMode:UIViewContentModeScaleAspectFit];
        self.layer.masksToBounds = true;
        [self setTitle:[btnText uppercaseString] forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:13.0];
        CGFloat spacing = 10; // the amount of spacing to appear between image and title
        self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
        UIImageView *btnImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 12,16 , 16)];
        btnImg.image = [UIImage imageNamed:btnImageName];
        [self addSubview:btnImg];
    }
    return self;

}


@end
