//
//  FrodoAlertView.h
//  Frodo
//
//  Created by Akanksha Sharma on 24/09/15.
//  Copyright (c) 2015 Nexgear Technology Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGAlertButton.h"

typedef void(^BtnClickedWithTag)(int tag);

@interface NGAlertView : UIView


@property (unsafe_unretained, nonatomic) IBOutlet NSLayoutConstraint *alertMessageBottomSpace;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonViewHeight;
@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonsViewWidth;


-(void) showAlertViewWithAlertMessage : (NSString *) alertMessage WithButtons : (NSMutableArray *) alertButtons withButtonClickedBlock : (BtnClickedWithTag) btnClickWithTag;

- (IBAction)closeButton:(id)sender;

@property (nonatomic,copy) BtnClickedWithTag btnClickedWithTag;
@property (nonatomic,copy) NSMutableArray *alertButtonsArray;
@property (nonatomic,copy) NSNumber *showAlertCloseButton;


@end
